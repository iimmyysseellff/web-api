<?php

namespace App\Listeners;

use App\Events\MachineLogged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMachineLoggedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MachineLogged  $event
     * @return void
     */
    public function handle(MachineLogged $event)
    {

    }
}
