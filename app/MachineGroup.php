<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineGroup extends Model
{
  protected $fillable = ['name'];

  public function machines()
  {
    return $this->hasMany(Machine::class);
  }
}
