<?php

namespace App;

use App\Utils\Constants;
use Illuminate\Database\Eloquent\Model;

class Machine extends Model
{
  protected $fillable = ['name'];

  public function machineLogs()
  {
    return $this->hasMany(MachineLog::class);
  }

  public function machineGroup()
  {
    return $this->belongsTo(MachineGroup::class, 'machine_group_id');
  }

  public function getFormattedName() {
    return ucwords(strtolower($this->name), " ");
  }

  public static function status($date)
  {
    if ($date == 'today')
      $date = date('Y-m-d');
    $reported = static::
    whereIn("id",
        collect(MachineLog::whereDate("created_at", $date)
            ->get(["machine_id"])
            ->toArray())
            ->flatten()
            ->toArray())
        ->get();

    $unReported = static::
    whereNotIn("id",
        collect(MachineLog::whereDate("created_at", $date)
            ->get(["machine_id"])
            ->toArray())
            ->flatten()
            ->toArray())
        ->get();

    return [Constants::$reportedIfTrueText => $reported,
        Constants::$reportedIfFalseText => $unReported];
  }

}
