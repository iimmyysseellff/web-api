<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MachineLog extends Model
{
  protected $guarded = ['id'];

  protected $casts = [
      'machine_id' => 'int'
  ];

  public function getFormattedMachineName()
  {
    return sprintf("%s %s", $this->machines->getFormattedName(), $this->machines->placeholder);
  }

  public function getLogDataAttribute($value)
  {
    return json_decode($value, true);
  }

  public function getOperatorAttribute($value)
  {
    return ucwords($value, ' ');
  }

  public static function getLogByDateGroupByMachineId($date, $groupId)
  {
    $logs = static::whereDate('created_at', $date)->get();
    $collectLogs = collect($logs);
    $filteredLogs = $collectLogs->filter(function ($item, $key) use ($groupId) {
      return $item->machines->machine_group_id == $groupId;
    });
    return $filteredLogs;
  }

  public static function groupLogToMachineName($date, $groupId)
  {
    $collect = collect(static::getLogByDateGroupByMachineId($date, $groupId));
    $mapped = $collect->mapToGroups(function ($item, $key) {
      return [$item->machines->name => $item];
    });
    return $mapped;
  }

  public function machines()
  {
    return $this->belongsTo(Machine::class, 'machine_id');
  }
}
