<?php

namespace App\Http\Controllers;

use App\Events\MachineLogged;
use App\Http\Requests\StoreMachineLogRequest;
use App\Machine;
use App\MachineLog;

class MachineLogController extends Controller
{

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(StoreMachineLogRequest $request)
  {
    $data = MachineLog::create($request->all());
    $data->machine = Machine::find($data->machine_id);
    event(new MachineLogged($data->toArray()));
    return $data;
  }

  public function log($date)
  {
    if ($date == 'today')
      $date = date("Y-m-d");
    return MachineLog::whereDate('created_at', $date)->with('machines')->get();
  }

}
