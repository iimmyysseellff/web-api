<?php

namespace App\Http\Controllers;

use App\Machine;
use App\Utils\Constants;

class MachineController extends Controller
{

  public function status($date)
  {
    return Machine::status($date);
  }

  public function index()
  {
    return Machine::all();
  }

  public function byCategory($date)
  {
    $status = Machine::status($date);

    foreach ($status as $key0 => $value0) {
      foreach ($value0 as $value) {
        $value['report_status'] = $key0 == Constants::$reportedIfTrueText;
      }
    }

    $mergeStatus = collect([$status['reported'], $status['unReported']])->flatten();
    $groupToMachineType = $mergeStatus->mapToGroups(function ($item, $key) {
      return [$item->machineGroup->name => $item];
    });

    return $groupToMachineType;
  }

}