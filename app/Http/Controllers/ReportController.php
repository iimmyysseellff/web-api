<?php

namespace App\Http\Controllers;

use App\MachineGroup;
use App\MachineLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ReportController extends Controller
{

  public function viewReport()
  {
    return view('auth.report.view');
  }

  public function getReport(Request $request)
  {
    $date = $request->input('date');
    $groupId = $request->input('group');
    $filteredLogs = MachineLog::groupLogToMachineName($date, $groupId);
    try {
      return view('auth.report.get')->with('logs', $filteredLogs)->render();
    } catch (\Throwable $e) {
      return $e;
    }
  }

  public function getPdfReport(Request $request)
  {
    $groupId = $request->post('group-id');
    $date = $request->post('date');
    $format = $request->post('format');
    $filteredLogs = MachineLog::groupLogToMachineName($date, $groupId);
    $group = MachineGroup::find($groupId);
    $steps = $group->time_log_interval;
    $groupName = $group->name;
    $title = sprintf("%s %s", $groupName, $date);
    $pdf = App::make('dompdf.wrapper');
    $data = compact('filteredLogs', 'steps', 'title', 'date', 'groupName');
    $filepdf = $pdf->loadView('auth.report.pdf', $data)->setPaper('legal', 'landscape');
    switch ($format) {
      case 'html':
        return view('auth.report.pdf', $data);
      case 'pdf':
        return $filepdf->stream(sprintf('Laporan %s.pdf', $title));
    }
  }

}
