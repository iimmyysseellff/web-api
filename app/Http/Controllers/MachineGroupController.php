<?php

namespace App\Http\Controllers;

use App\MachineGroup;

class MachineGroupController extends Controller
{
  /*
   * Will return all machine group data
   */
  public function index()
  {
    return MachineGroup::all();
  }
}
