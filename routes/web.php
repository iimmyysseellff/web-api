<?php

Route::get('/', function () {
  return redirect('home');
});

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::prefix('frontend/auth')->middleware('auth')->group(function () {
  Route::get('view-report', 'ReportController@viewReport')->name('view.report');
  Route::post('get-report/', 'ReportController@getReport');
  Route::post('get-report/pdf', 'ReportController@getPdfReport')->name('get.report');
});

Route::prefix('api')->group(function () {

  Route::prefix('machine')->group(function () {
    Route::get('status/{date}', 'MachineController@status')->name('machine.status');
    Route::get('status/{date}/bycategory', 'MachineController@byCategory');
  });

  Route::prefix('machinelog')->group(function () {
    Route::post('log', 'MachineLogController@store')->name('machinelog.store');
    Route::get('log/{date}', 'MachineLogController@log')->name('machine.log');
  });

  Route::resource('machine', 'MachineController', ['only' => ['index']]);
  Route::resource('machine-group', 'MachineGroupController', ['only' => 'index']);

});