function resetRowOfThisTbody(tbody) {
  Array.from(tbody.children).forEach(function (value) {
    value.remove()
  });
}

var notifSound = document.getElementById("notifSound");

function pling() {
  notifSound.play();
}