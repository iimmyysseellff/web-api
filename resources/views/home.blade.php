@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Notifikasi log mesin</h5>
          </div>
          <table class="table table-hover" id="arrived-machine-table">
            <thead class="thead-dark">
            <tr>
              <th scope="col">Mesin</th>
              <th scope="col">Oleh</th>
              <th scope="col">Tanggal</th>
            </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><span id="data-title">Modal Heading</span></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <table class="table" id="data-detail">
          <tbody>
          </tbody>
        </table>
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>
@endsection
@section('js')
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script src="{{ asset('js/notify.min.js') }}"></script>
  <script>
    var pusher = new Pusher('521e9ef201e70d53c754', {
      cluster: 'ap1',
      encrypted: true
    });
    let channel = pusher.subscribe('machine-event');
    let arrivedMachineTableBody = document.getElementById("arrived-machine-table").getElementsByTagName("tbody")[0];
    let dataDetailTableBody = document.getElementById("data-detail").getElementsByTagName("tbody")[0];

    let showModal = function (dom) {
      if (dataDetailTableBody.childElementCount > 0)
        resetRowOfThisTbody(dataDetailTableBody);
      title = dom.getAttribute("title");
      content = JSON.parse(dom.getAttribute("data-detail"));
      $("#data-title").text(title);
      Object.entries(content).forEach(function (item, key) {
        row = dataDetailTableBody.insertRow(key);
        leftCell = row.insertCell(0);
        leftCell.innerText = item[0];
        leftCell.className += ' text-uppercase';
        leftCell.className += ' font-weight-bold';
        row.insertCell(1).innerHTML = item[1];
      });
      $("#myModal").modal();
    };

    channel.bind('App\\Events\\MachineLogged', function (data) {
      $.notify("Data mesin baru telah sampai", {className: 'info', position: 'bottom-left'});
      pling();
      row0 = arrivedMachineTableBody.insertRow(0);
      let placeholder = data.data.machine.placeholder || "";
      let title = data.data.machine.name + " " + placeholder;
      row0.insertCell(0).innerText = title;
      row0.insertCell(1).innerText = data.data.operator;
      row0.insertCell(2).innerText = data.data.created_at;
      row0.setAttribute("data-detail", JSON.stringify(data.data.log_data));
      row0.setAttribute("title", title);
      row0.setAttribute("onclick", "showModal(this)");
      row0.className += " cursor-hand";
    });
  </script>
@endsection
