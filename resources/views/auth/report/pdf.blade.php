<html>
<head>
  <title>Laporan - {{ $title }}</title>
  <style>
    body {
      font-family: Calibri, Candara, Segoe, "Segoe UI", Optima, Arial, sans-serif;
    }

    table {
      border-collapse: collapse
    }

    table, th, td {
      border: 1px solid black;
      text-align: center;
    }

    .machine-name {
      font-size: 12px;
    }

    .machine-placeholder {
      font-size: 11px;
    }

    .machine-log-data {
      font-size: 10px;
    }

    .img-cop {
      margin-bottom: 5px;
      line-height: .5;
    }
  </style>
</head>
<body>
@if(count($filteredLogs) <= 0)
  <div class="alert-danger alert" style="margin: 10px;">
    Tidak ada data!
  </div>
@else
  <div class="img-cop">
    <img height="70px" src="{{ asset('img/logo.jpg') }}" alt="logo" align="left">
    <h1>{{ $groupName }}</h1>
    <h4>Laporan tanggal: {{ $date }}</h4>
  </div>

  <table border="1" cellpadding="5">
    <thead>
    <tr>
      <th class="machine-name" rowspan="3">JAM</th>
      @foreach($filteredLogs as $key => $log)
        <th class="machine-name"
            colspan="{{ count(collect($log)->mapToGroups(function ($item, $key) {
          return [$item->machines->placeholder => $item];
          })) * count($log[0]->log_data) }}"
            @if($log[0]->machines->placeholder == null)
            rowspan="2"
            @endif>
          {{ $key }}
        </th>
      @endforeach
    </tr>
    <tr>
      @foreach($filteredLogs as $key => $logs)
        @foreach(collect($logs)->mapToGroups(function ($item, $key) {
          return [$item->machines->placeholder => $item];
        }) as $key => $log)
          @if($key != null)
            <th class="machine-placeholder" colspan="{{ count($log[0]->log_data) }}">{{ $key }}</th>
          @endif
        @endforeach
      @endforeach
    </tr>
    <tr>
      @foreach($filteredLogs as $key => $logs)
        @foreach(collect($logs)->mapToGroups(function ($item, $key) {
          return [$item->machines->placeholder => $item];
        }) as $key => $log)
          @foreach($log[0]->log_data as $key => $value)
            <th class="machine-log-data">{{ $key }}</th>
          @endforeach
        @endforeach
      @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach(range(0, 23, $steps) as $jam)
      <tr>
        <td class="machine-log-data">{{ sprintf("%02d", $jam) }}.00</td>
        @foreach($filteredLogs as $key => $logs)
          @foreach(collect($logs)->mapToGroups(function ($item, $key) {
            return [$item->machines->placeholder => $item];
          }) as $key => $log)
            @foreach(collect($log)->filter(function ($item, $key) use ($jam) {
              return $item->created_at->hour == $jam;
            })->first()->log_data as $key => $value )
              <td class="machine-log-data">
                {{ $value }}
              </td>
            @endforeach
          @endforeach
        @endforeach
      </tr>
    @endforeach
    </tbody>
  </table>
@endif
</body>
</html>