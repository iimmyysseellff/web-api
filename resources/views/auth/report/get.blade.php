@if($logs->count() <= 0)
  <div class="alert-danger alert" style="margin: 10px;">
    Tidak ada data!
  </div>
@else
  <tbody>
  @foreach($logs as $log)
    <tr log-data="{{ json_encode($log->log_data) }}" class="change-cursor-to-hand" onclick="showData(this)">
      <td>{{ $loop->iteration }}</td>
      <th>{{ $log->getFormattedMachineName() }}</th>
      <th>{{ $log->operator }}</th>
    </tr>
  @endforeach
  </tbody>
@endif