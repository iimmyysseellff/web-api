@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            <div class="card-title">
              <h3>Lihat laporan log</h3>
              <form id="form-laporan-log">
                @csrf
                <div class="form-row align-items-center">
                  <div class="col-auto">
                    <label class="sr-only" for="inlineFormInputGroup">Tanggal</label>
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Tanggal</div>
                      </div>
                      <input type="date" required name="date" class="form-control" id="inlineFormInputGroup"
                             placeholder="Dari">
                    </div>
                  </div>
                  <div class="col-auto">
                    <label class="sr-only" for="inlineFormInputGroup">Grup</label>
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Grup</div>
                      </div>
                      <select class="form-control" name="group-id">
                        @foreach(\App\MachineGroup::all() as $value)
                          <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <input type="hidden" id="format" name="format" value="html">
                  <div class="col-auto">
                    <button type="submit" class="btn btn-primary mb-2">Tampilkan</button>
                  </div>
                  <div class="col-auto">
                    <button type="button" class="btn btn-primary mb-2" onclick="downloadPdf()"> Download PDF</button>
                  </div>
                  <div class="col-auto">
                    <span class="text-muted" id="loading-indicator"></span>
                  </div>
                </div>
              </form>
              <form method="post" action="{{ route('get.report') }}" id="pdf-report-form">
                @csrf
                <input type="hidden" name="group-id">
                <input type="hidden" name="date">
                <input type="hidden" name="format" value="pdf">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row" style="margin-top: 10px">
      <div class="col-sm-12">
        <div class="card">
          <div class="card-body">
            {{--<h5 class="card-title">--}}
            {{--Tabel log data--}}
            {{--</h5>--}}
            <div class="table-responsive">
              <div id="report-html"></div>
              {{--<table class="table table-hover" id="table-log-data">--}}
              {{--<thead class="thead-dark">--}}
              {{--<tr>--}}
              {{--<th>#</th>--}}
              {{--<th>Mesin</th>--}}
              {{--<th>Diinput Oleh</th>--}}
              {{--</tr>--}}
              {{--</thead>--}}
              {{--<tbody></tbody>--}}
              {{--</table>--}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
  <script src="http://danml.com/js/download.js"></script>
  <script>

    var loadingIndicator = $("#loading-indicator");

    function downloadPdf() {
      let groupId = $("select[name='group-id']").val();
      let date = $("input[name='date']").val();
      let title = "Laporan " + groupId + " - " + date + ".pdf";

      $("#pdf-report-form>input[name=group-id]")[0].value = groupId;
      $("#pdf-report-form>input[name=date]")[0].value = date;

      $("#pdf-report-form").submit();
    }

    $("#form-laporan-log").submit(function (e) {
      e.preventDefault();
      loadingIndicator.text("Sedang memuat data ...");
      $.ajax({
        url: '{{ route('get.report') }}',
        type: 'post',
        data: $(this).serialize(),
        success: function (r) {
          loadingIndicator.text("");
          $("#report-html").html(r);
          // $("#table-log-data")[0].tBodies[0].innerHTML = r;
        },
        error: function (r) {
          loadingIndicator.text("");
          alert(r)
        }
      })
    });

    function showData(rowObject) {
      logDataTableBody = $('table')[1].tBodies[0];
      logData = JSON.parse(rowObject.getAttribute("log-data"));

      $("#machine-name").text("mesin " + rowObject.children[1].textContent);

      if (logDataTableBody.childElementCount > 0)
        resetRowOfThisTbody(logDataTableBody);

      Object.entries(logData).forEach(function (item, key) {
        row = logDataTableBody.insertRow(key);
        leftCell = row.insertCell(0);
        leftCell.innerText = item[0];
        leftCell.className += ' text-uppercase';
        leftCell.className += ' font-weight-bold';
        row.insertCell(1).innerHTML = item[1];
      });
    }
  </script>
@endsection