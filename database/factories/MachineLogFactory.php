<?php

use App\Machine;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\MachineLog::class, function (Faker $faker) {
  return array(
      'machine_id' => random_int(Machine::where('machine_group_id', 1)->min("id"), Machine::where('machine_group_id', 1)->max("id")),
      'log_data' => json_encode(array('C' => 40, 'Heat' => 50)),
      'operator' => $faker->name(),
      'created_at' => Carbon::now()
  );
});

$factory->define(App\MachineLog::class, function (Faker $faker) {
  return array(
      'machine_id' => random_int(Machine::where('machine_group_id', 2)->min("id"), Machine::where('machine_group_id', 2)->max("id")),
      'log_data' => json_encode(array('C' => 40, 'Heat' => 50)),
      'operator' => $faker->name(),
      'created_at' => Carbon::now()
  );
});

$factory->define(App\MachineLog::class, function (Faker $faker) {
  return array(
      'machine_id' => random_int(Machine::where('machine_group_id', 3)->min("id"), Machine::where('machine_group_id', 3)->max("id")),
      'log_data' => json_encode(array('C' => 40, 'Heat' => 50)),
      'operator' => $faker->name(),
      'created_at' => Carbon::now()
  );
});