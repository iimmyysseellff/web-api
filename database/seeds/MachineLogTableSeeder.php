<?php

use App\Machine;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MachineLogTableSeeder extends Seeder
{

  private $faker;

  public function __construct(Faker\Generator $faker)
  {
    $this->faker = $faker;
  }

  /**
   * Run the database seeds.
   *
   * @return void
   */

  private function incrementMachineIdOfGroup($group)
  {
    $max = DB::select(DB::raw("SELECT max(machine_id) as max from machine_logs, machines where machine_id=machines.id and machine_group_id=${group}"))[0]->max;
    if (!($max == Null))
      return $max + 1;
    return Machine::where("machine_group_id", $group)->min("id");
  }

  private function incrementHour($group)
  {
    $logs = \App\MachineLog::whereDate('created_at', date("Y-m-d"))->get();
    $collectLogs = collect($logs);
    $getByGroup = $collectLogs->filter(function ($item, $key) use ($group) {
      return $item->machines->machine_group_id == $group;
    });
    $lastLog = $getByGroup->last();
    if ($lastLog == null)
      return Carbon::now();
    return $lastLog->created_at->addHour();
  }

  public function dataStructure($group): array
  {
    $incrementedId = $this->incrementMachineIdOfGroup($group);
    $incrementHour = $this->incrementHour($group);
    return [
        'machine_id' => $incrementedId,
        'log_data' => json_encode(array('Temp' => random_int(30, 50), 'Heat' => random_int(40, 60))),
        'operator' => $this->faker->name(),
        'created_at' => $incrementHour
    ];
  }

  public function run()
  {
    foreach (Machine::all() as $machine) {
      foreach (range(0, 23) as $jam) {
        DB::table('machine_logs')->insert([
            'machine_id' => $machine->id,
            'log_data' => json_encode(array('Temp' => random_int(30, 50), 'Heat' => random_int(40, 60))),
            'operator' => $this->faker->name(),
            'created_at' => sprintf("%s %s:%s", date("Y-m-d"), sprintf("%02d", $jam), date("i:s"))
        ]);
      }
    }
//    foreach ([1, 2, 3] as $group):
//      foreach (range(0, Machine::where('machine_group_id', $group)->count() - 1) as $_):
//        DB::table("machine_logs")->insert($this->dataStructure($group));
//      endforeach;
//    endforeach;
  }
}
