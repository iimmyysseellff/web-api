<?php

use Illuminate\Database\Seeder;
use App\Utils\MachinesInit;

class MachinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('machines')->insert(MachinesInit::$data);
    }
}
