<?php

use Illuminate\Database\Seeder;

class MachineGroupsTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('machine_groups')->insert([
        ['name' => 'Boiler Lokal Unit #3', 'id' => 1, 'time_log_interval' => 1],
        ['name' => 'Lokal Turbin Unit #3', 'id' => 2, 'time_log_interval' => 2],
        ['name' => 'Panel Lokal Turbin Unit #3', 'id' => 3, 'time_log_interval' => 2]
    ]);
  }
}
