<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMachinesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (!Schema::hasTable('machines')) {
      Schema::create('machines', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('placeholder', 10)->nullable();
        $table->string('classname')->nullable();
        $table->integer('machine_group_id')->unsigned();
        $table->foreign('machine_group_id')->references('id')->on('machine_groups');
        $table->timestamps();
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('machines');
  }
}
