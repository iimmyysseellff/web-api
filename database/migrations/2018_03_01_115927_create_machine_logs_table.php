<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMachineLogsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    if (!Schema::hasTable('machine_logs')) {
      Schema::create('machine_logs', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('machine_id')->unsigned();
        $table->text('log_data');
        $table->string("operator");
        $table->timestamps();
        $table->foreign('machine_id')->references('id')->on('machines');
      });
    }
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('machine_logs');
  }
}
